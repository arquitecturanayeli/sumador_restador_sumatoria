library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SumadorRestadorAnticipadoSumatoria is
    generic(
		i:integer:=4);
		
	port(
		A,B: in std_logic_vector(i-1 downto 0);
		S: out std_logic_vector(i-1 downto 0);
		selector: in std_logic;
		Co: out std_logic);
		
end SumadorRestadorAnticipadoSumatoria;

architecture Behavioral of SumadorRestadorAnticipadoSumatoria is
	signal C: std_logic_vector(i downto 0);
	signal G,P,EB: std_logic_vector(i-1 downto 0);
	
begin
	C(0)<=selector;
	
	oper1:for n in 0 to i-2 generate
		oper2: for j in n+1 to i-1 generate
			G(n) and P(j); --algo 1
		end generate oper2;
	end generate oper1;
	
	oper3: for n in 0 to i-1 generate
		C(0) and P(n) --algo 2
	end generate oper3;
		
		C(n)<= --algo 3
		Co<= C(n);
end Behavioral;
